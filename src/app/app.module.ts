import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// ngx-Bootstrap
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';


import { AppComponent } from './app.component';
//Routing
import { AppRoutes } from "./app.routing";

// Components
import { LoginComponent } from './login/login.component';
import { LayoutComponent } from './layout/layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdvertisersComponent } from './advertisers/advertisers/advertisers.component';
import { AdvertisersSponsorsAdsComponent } from './advertisers/advertisers-sponsors-ads/advertisers-sponsors-ads.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LayoutComponent,
    DashboardComponent,
    AdvertisersComponent,
    AdvertisersSponsorsAdsComponent
  ],
  imports: [
    BrowserModule,
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    // DataTableModule,
    RouterModule.forRoot(AppRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
