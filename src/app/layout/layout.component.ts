import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  settingMenuActive= false;
  showNavbarMenu=false;
  constructor() { }

  ngOnInit() {
  }
  toggleSettingMenu(){
    this.settingMenuActive = !this.settingMenuActive;
  }
  toggleNavbarMenu(){
    this.showNavbarMenu = !this.showNavbarMenu;
  }
}
