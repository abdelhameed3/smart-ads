
import { Routes } from "@angular/router";
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdvertisersComponent } from './advertisers/advertisers/advertisers.component';
import { AdvertisersSponsorsAdsComponent } from './advertisers/advertisers-sponsors-ads/advertisers-sponsors-ads.component';


export const AppRoutes: Routes = [
  {
    path: "login",
    component: LoginComponent,
  },
  {
    path: "dashboard",
    component: DashboardComponent,
  },
  {
    path:"advertisers"  ,
    component: AdvertisersComponent
  },
  {
    path:"advertisers-sponsors-ads"  ,
    component: AdvertisersSponsorsAdsComponent
  }
];