import { Component,OnInit, TemplateRef, ViewEncapsulation, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-advertisers-sponsors-ads',
  templateUrl: './advertisers-sponsors-ads.component.html',
  styleUrls: ['./advertisers-sponsors-ads.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AdvertisersSponsorsAdsComponent implements OnInit {

  @ViewChild('staticTabs', { static: false }) staticTabs: TabsetComponent;
 
  selectTab(tabId: number) {
    this.staticTabs.tabs[tabId].active = true;
  }
  advertisersSponserAds = [
    {name:"Consectetur gravida est ",createdDate:"08/03/1970",noAds:"1253",views:"4,415",clicks:"4,415",shares:"4,234",status:"active"},
    {name:"Vestibulum adipiscing ",createdDate:"25/05/1996",noAds:"2770",views:"2,147",clicks:"2,147",shares:"1,700",status:"inactive"},
    {name:"Dolor proin  ",createdDate:"15/09/1958",noAds:"3468",views:"4,233",clicks:"4,233",shares:"4,075",status:"active"},
    {name:"Consectetur gravida est ",createdDate:"08/03/1970",noAds:"1253",views:"4,415",clicks:"4,415",shares:"4,234",status:"active"},
    {name:"Vestibulum adipiscing ",createdDate:"25/05/1996",noAds:"2770",views:"2,147",clicks:"2,147",shares:"1,700",status:"inactive"},
    {name:"Dolor proin  ",createdDate:"15/09/1958",noAds:"3468",views:"4,233",clicks:"4,233",shares:"4,075",status:"active"},
    {name:"Consectetur gravida est ",createdDate:"08/03/1970",noAds:"1253",views:"4,415",clicks:"4,415",shares:"4,234",status:"active"},
    {name:"Vestibulum adipiscing ",createdDate:"25/05/1996",noAds:"2770",views:"2,147",clicks:"2,147",shares:"1,700",status:"inactive"},
    {name:"Dolor proin  ",createdDate:"15/09/1958",noAds:"3468",views:"4,233",clicks:"4,233",shares:"4,075",status:"active"},
    {name:"Consectetur gravida est ",createdDate:"08/03/1970",noAds:"1253",views:"4,415",clicks:"4,415",shares:"4,234",status:"active"},
    {name:"Vestibulum adipiscing ",createdDate:"25/05/1996",noAds:"2770",views:"2,147",clicks:"2,147",shares:"1,700",status:"inactive"},
    {name:"Dolor proin  ",createdDate:"15/09/1958",noAds:"3468",views:"4,233",clicks:"4,233",shares:"4,075",status:"active"},
    {name:"Consectetur gravida est ",createdDate:"08/03/1970",noAds:"1253",views:"4,415",clicks:"4,415",shares:"4,234",status:"active"},
    {name:"Vestibulum adipiscing ",createdDate:"25/05/1996",noAds:"2770",views:"2,147",clicks:"2,147",shares:"1,700",status:"inactive"},
    {name:"Dolor proin  ",createdDate:"15/09/1958",noAds:"3468",views:"4,233",clicks:"4,233",shares:"4,075",status:"active"},
    {name:"Consectetur gravida est ",createdDate:"08/03/1970",noAds:"1253",views:"4,415",clicks:"4,415",shares:"4,234",status:"active"},
    {name:"Vestibulum adipiscing ",createdDate:"25/05/1996",noAds:"2770",views:"2,147",clicks:"2,147",shares:"1,700",status:"inactive"},
    {name:"Dolor proin  ",createdDate:"15/09/1958",noAds:"3468",views:"4,233",clicks:"4,233",shares:"4,075",status:"active"},
    
  ]
   // modal
   modal;
   modalRef: BsModalRef;
   config = {
     animated: true
   };
   // photo
   uploadedFile: any;
   public fileData: any = null;
  constructor(private modalService: BsModalService, private _sanitizer: DomSanitizer,) { }

  ngOnInit() {
  }

  // modal
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, Object.assign({}, { class: ' modal-dialog-centered modal-lg' }) );
  }

  // upload
  uploadFile($event){
    console.log($event);
    this.fileData = $event.target.files[0];;
    this.uploadedFile = this._sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(this.fileData));

  }
  clearFile(){
    this.fileData = null;
    this.uploadedFile = null;

  }


}
