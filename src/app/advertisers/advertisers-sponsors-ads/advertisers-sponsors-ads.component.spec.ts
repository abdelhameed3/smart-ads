import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertisersSponsorsAdsComponent } from './advertisers-sponsors-ads.component';

describe('AdvertisersSponsorsAdsComponent', () => {
  let component: AdvertisersSponsorsAdsComponent;
  let fixture: ComponentFixture<AdvertisersSponsorsAdsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertisersSponsorsAdsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertisersSponsorsAdsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
