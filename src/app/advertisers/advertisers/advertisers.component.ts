import { Component,OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-advertisers',
  templateUrl: './advertisers.component.html',
  styleUrls: ['./advertisers.component.scss']
})
export class AdvertisersComponent implements OnInit {

  advertisers= [
    {photo:"../../../assets/images/logo-1.png",name:"Consectetur gravida est ",createdDate:"08/03/1970",noAds:"1253",views:"4,415",clicks:"4,415",shares:"4,234",status:"active"},
    {photo:"../../../assets/images/logo-2.png",name:"Vestibulum adipiscing ",createdDate:"25/05/1996",noAds:"2770",views:"2,147",clicks:"2,147",shares:"1,700",status:"inactive"},
    {photo:"../../../assets/images/logo-3.png",name:"Dolor proin  ",createdDate:"15/09/1958",noAds:"3468",views:"4,233",clicks:"4,233",shares:"4,075",status:"active"},
    {photo:"../../../assets/images/logo-1.png",name:"Consectetur gravida est ",createdDate:"08/03/1970",noAds:"1253",views:"4,415",clicks:"4,415",shares:"4,234",status:"active"},
    {photo:"../../../assets/images/logo-2.png",name:"Vestibulum adipiscing ",createdDate:"25/05/1996",noAds:"2770",views:"2,147",clicks:"2,147",shares:"1,700",status:"inactive"},
    {photo:"../../../assets/images/logo-3.png",name:"Dolor proin  ",createdDate:"15/09/1958",noAds:"3468",views:"4,233",clicks:"4,233",shares:"4,075",status:"active"},
    {photo:"../../../assets/images/logo-1.png",name:"Consectetur gravida est ",createdDate:"08/03/1970",noAds:"1253",views:"4,415",clicks:"4,415",shares:"4,234",status:"active"},
    {photo:"../../../assets/images/logo-2.png",name:"Vestibulum adipiscing ",createdDate:"25/05/1996",noAds:"2770",views:"2,147",clicks:"2,147",shares:"1,700",status:"inactive"},
    {photo:"../../../assets/images/logo-3.png",name:"Dolor proin  ",createdDate:"15/09/1958",noAds:"3468",views:"4,233",clicks:"4,233",shares:"4,075",status:"active"},
    {photo:"../../../assets/images/logo-1.png",name:"Consectetur gravida est ",createdDate:"08/03/1970",noAds:"1253",views:"4,415",clicks:"4,415",shares:"4,234",status:"active"},
    {photo:"../../../assets/images/logo-2.png",name:"Vestibulum adipiscing ",createdDate:"25/05/1996",noAds:"2770",views:"2,147",clicks:"2,147",shares:"1,700",status:"inactive"},
    {photo:"../../../assets/images/logo-3.png",name:"Dolor proin  ",createdDate:"15/09/1958",noAds:"3468",views:"4,233",clicks:"4,233",shares:"4,075",status:"active"},
    {photo:"../../../assets/images/logo-1.png",name:"Consectetur gravida est ",createdDate:"08/03/1970",noAds:"1253",views:"4,415",clicks:"4,415",shares:"4,234",status:"active"},
    {photo:"../../../assets/images/logo-2.png",name:"Vestibulum adipiscing ",createdDate:"25/05/1996",noAds:"2770",views:"2,147",clicks:"2,147",shares:"1,700",status:"inactive"},
    {photo:"../../../assets/images/logo-3.png",name:"Dolor proin  ",createdDate:"15/09/1958",noAds:"3468",views:"4,233",clicks:"4,233",shares:"4,075",status:"active"},
    {photo:"../../../assets/images/logo-1.png",name:"Consectetur gravida est ",createdDate:"08/03/1970",noAds:"1253",views:"4,415",clicks:"4,415",shares:"4,234",status:"active"},
    {photo:"../../../assets/images/logo-2.png",name:"Vestibulum adipiscing ",createdDate:"25/05/1996",noAds:"2770",views:"2,147",clicks:"2,147",shares:"1,700",status:"inactive"},
    {photo:"../../../assets/images/logo-3.png",name:"Dolor proin  ",createdDate:"15/09/1958",noAds:"3468",views:"4,233",clicks:"4,233",shares:"4,075",status:"active"},
    
  ]
  // modal
  modal;
  modalRef: BsModalRef;
  config = {
    animated: true
  };
  // photo
  uploadedFile: any;
  public fileData: any = null;
  

  constructor(
    private modalService: BsModalService,
    private _sanitizer: DomSanitizer,
    ) {
  }
 
  
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, Object.assign({}, { class: ' modal-dialog-centered modal-lg' }) );
  }

  ngOnInit() {
  }
  // upload
  uploadFile($event){
    // console.log($event);
    this.fileData = $event.target.files[0];;
    this.uploadedFile = this._sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(this.fileData));

  }
  clearFile(){
    this.fileData = null;
    this.uploadedFile = null;

  }
}
